About this module
------
Module that allows Achievements to be sent to a Mozilla Open Badges Backpack.

Author
------
Royce Kimmons
royce@kimmonsdesign.com

Acknowledgements
------
Backpack icon is a derivative of Exquisite-backpack by Sasa Stefanovic at 
http://commons.wikimedia.org/wiki/File:Exquisite-backpack.png and is licensed under GNU GPL 3.

Installation
------------
Copy the Achievements Open Badges Bridge module directory to your modules directory and then 
enable on the admin modules page.  The Achievements module by Mobius Iff must already be enabed.